import logging.config
import os
from multipledispatch import dispatch


class ConfigLogger:
    baseNameLogger = "app"
    pathLoggerDir = "log"
    nameFileLogger = "fast1.log"

    @staticmethod
    def createLogDir(pathDir="log"):
        if not os.path.exists(pathDir):
            os.mkdir(pathDir)

    @staticmethod
    def settingLogger():
        ConfigLogger.createLogDir(pathDir=ConfigLogger.pathLoggerDir)
        logging.config.dictConfig(logging_config)
        logging.basicConfig(level=logging.INFO, handlers=[])

    @staticmethod
    @dispatch(str)
    def getLogger(nameMod):
        return logging.getLogger(ConfigLogger.baseNameLogger + "." + nameMod)

    @staticmethod
    @dispatch(str, str)
    def getLogger(nameBase, nameMod):
        return logging.getLogger(nameBase + "." + nameMod)

    # def templates_logging():
    #     logFC = ConfigLogger.getLogger("FileStdout", "name1")
    #     logF = ConfigLogger.getLogger("OnlyFile", "name2")
    #     logFC.info("Пример использования запись в файл и консоль")
    #     logF.info("Пример использования запись только в файл")


logging_config = \
    {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "form1": {
                "format": "/* %(asctime)s - %(module)s.%(funcName)s(%(lineno)d) - [%(threadName)s] - %(thread)d - [%(processName)s] - %(process)d */ -> \n%(levelname)s: %(message)s"
            },
            "form2": {
                "format": "/* %(asctime)s - %(module)s.%(funcName)s(%(lineno)d) - [%(threadName)s] - [%(thread)d] */  \n%(levelname)s: %(message)s"
            },
            "form3": {
                "format": "/* %(asctime)s - %(module)s.%(funcName)s(%(lineno)d) - %(name)s */ -> \n%(levelname)s: %(message)s"
            },
            "form4": {
                "format": "/* %(asctime)s - %(module)s.%(funcName)s(%(lineno)d) - [%(processName)s] - %(process)d */ -> \n%(levelname)s: %(message)s"
            },
            "con1": {
                "format": "%(asctime)s - %(module)s.%(funcName)s(%(lineno)d) - [%(threadName)s] - [%(thread)d] \n > %(levelname)s: %(message)s"
            },
            "con2": {
                "format": "%(message)s"
            }
        },
        "handlers": {
            "rotating_file1": {
                "class": "logging.handlers.RotatingFileHandler",
                "level": "DEBUG",
                "formatter": "form2",
                "filename": f"{ConfigLogger.pathLoggerDir}/{ConfigLogger.nameFileLogger}",
                "maxBytes": 1048576,
                "backupCount": 20
            },
            "console1": {
                "class": "logging.StreamHandler",
                "level": "INFO",
                "formatter": "con1",
                "stream": "ext://sys.stdout"
            }
        },
        "loggers": {
            "app": {
                "handlers": ["console1"],
                "level": "DEBUG"
            },
            "FileStdout": {
                "handlers": ["rotating_file1", "console1"],
                "level": "DEBUG"
            },
            "OnlyFile": {
                "handlers": ["rotating_file1"],
                "level": "DEBUG"
            }
        }
    }
