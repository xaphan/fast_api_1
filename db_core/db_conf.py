from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from config import DATABASE_URL

from db_core.base import Base


class SessionDB:
    engine = create_engine(DATABASE_URL, pool_pre_ping=True, echo=True)

    sessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

    @staticmethod
    def get_models():
        return Base

    @staticmethod
    def get_db():
        """Dependency for getting session"""
        db = SessionDB.sessionLocal()
        try:
            yield db
        finally:
            db.close()
